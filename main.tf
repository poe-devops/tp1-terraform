resource "docker_image" "nginx" {
  name = "nginx:latest"
}

resource "docker_container" "nginx" {
  name  = var.container_name
  image = docker_image.nginx.image_id

  ports {
    internal = 80
    external = var.external_port
  }

  mounts {
    type = "bind"
    source = abspath("${path.module}/web-nginx")
    target = "/usr/share/nginx/html"
  }
}

resource "docker_image" "apache" {
  name = "httpd:latest"
}

resource "docker_container" "httpd" {
  name = var.container_name_httpd
  image = docker_image.apache.image_id
  ports {
    internal = 80
    external = var.external_port_apache
  }
  mounts {
    type = "bind"
    source = abspath("${path.module}/web-apache")
    target = "/usr/local/apache2/htdocs/"
  }
}
