variable "container_name" {
  description = "The name of the Docker container"
  type        = string
  default     = "nginx_container"
}

variable "external_port" {
  description = "The external port to bind to the Docker container"
  type        = number
  default     = 8000
}

variable "container_name_httpd" {
  description = "The name of the Docker container"
  type        = string
  default     = "apache_container"
}

variable "external_port_apache" {
  description = "The external port to bind to the Docker container"
  type        = number
  default     = 8001
}
