output "nginx_container_ip" {
  description = "The IP address of the Nginx container"
  value       = docker_container.nginx.network_data[0].ip_address
}

output "apache_container_ip" {
  description = "The IP address of the Apache container"
  value       = docker_container.httpd.network_data[0].ip_address
}